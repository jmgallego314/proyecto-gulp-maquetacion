'use strict';

const prepareContext = require('./lib/prepareContext');
const { renderUikit } = require('./lib/renderUikit');

const colors = require('./lib/annotations/colors');
const example = require('./lib/annotations/example');
const font = require('./lib/annotations/font');
const icons = require('./lib/annotations/icons');
const name = require('./lib/annotations/name');
const ratios = require('./lib/annotations/ratios');
const sizes = require('./lib/annotations/sizes');

/**
 * Actual theme function. It takes the destination directory `dest`,
 * and the context variables `ctx`.
 */
const uikit = (dest, ctx) =>
  prepareContext(ctx).then(preparedContext =>
    renderUikit(dest, preparedContext)
  );

uikit.annotations = [icons, colors, sizes, ratios, font, example, name];

// make sure sassdoc will preserve comments not attached to Sass
uikit.includeUnknownContexts = true;

module.exports = uikit;
